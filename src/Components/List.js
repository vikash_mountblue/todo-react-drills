import React, { Component } from "react";
import todoList from "./todoList";
import ListRender from "./ListRender";

class List extends Component {
  constructor() {
    super();

    this.state = {
      todoLists: todoList,
      todo: todoList.length,
      done: 0,
    };
  }
  doneCount = (data, index) => {
    todoList[index - 1].done = data;
    if (data) {
      this.setState({
        done: this.state.done + 1,
        todo: this.state.todo - 1,
        todoLists: todoList,
      });
    } else {
      this.setState({
        done: this.state.done - 1,
        todo: this.state.todo + 1,
        todoLists: todoList,
      });
    }
  };
  render() {
    let listItemsNotChecked = this.state.todoLists.map((data) => {
      if (!data.done) {
        return (
          <ListRender
            key={data.id}
            todo={data}
            index={data.id}
            completeHandler={this.doneCount}
          />
        );
      }
    });
    let listItemsChecked = this.state.todoLists.map((data) => {
      if (data.done) {
        return (
          <ListRender
            key={data.id}
            todo={data}
            index={data.id}
            completeHandler={this.doneCount}
          />
        );
      }
    });
    return (
      <div>
        <div className="result">
          <h2>Todo:{this.state.todo}</h2>
          <h2>Done:{this.state.done}</h2>
        </div>
        {listItemsNotChecked}
        {listItemsChecked}
      </div>
    );
  }
}

export default List;
