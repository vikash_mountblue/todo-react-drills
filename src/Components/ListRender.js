import React, { Component } from "react";

class ListRender extends Component {
  constructor(props) {
    super();

    this.state = {
      item: props.todo.item,
      done: props.todo.done,
    };
  }
  toggleCheckbox = () => {
    this.setState(
      {
        done: !this.state.done,
      },
      () => {
        // console.log("data", this.state.done);
        this.props.completeHandler(this.state.done, this.props.index);
      }
    );
  };

  render() {
    let listColor = this.state.done ? "done" : "notDone";
    return (
      <div className={`${listColor} list`}>
        <input
          type="checkbox"
          className="checkboxStyle"
          onClick={this.toggleCheckbox}
        ></input>
        {this.state.item}
      </div>
    );
  }
}

export default ListRender;
