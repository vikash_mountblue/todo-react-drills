const todoList = [
  {
    id: 1,
    item: "Transform this item list into React component(s)",
    done: false,
  },
  {
    id: 2,
    item: "Align it to the left",
    done: false,
  },
  {
    id: 3,
    item: "Separate data from the component(s).",
    done: false,
  },
  {
    id: 4,
    item:
      "Turn list items into todos (add checkboxes, completed should be colored gray and crossed out).",
    done: false,
  },
  {
    id: 5,
    item: "Item should be updated when checkbox is clikcked.",
    done: false,
  },
  {
    id: 6,
    item: "Order items By name and move checked to the bottom",
    done: false,
  },
  {
    id: 7,
    item: "Add counter to the top of the list (todo N, done M)",
    done: false,
  },
  {
    id: 8,
    item:
      "Get the list data with async function (delay for 1000 ms, make sure rendering works fine)",
    done: false,
  },
];
export default todoList;
// const todoList = [
//   "Transform this item list into React component(s)",
//   "Align it to the left",
//   "Separate data from the component(s).",
//   "Turn list items into todos (add checkboxes, completed should be colored gray and crossed out).",
//   "Item should be updated when checkbox is clikcked.",
//   "Order items By name and move checked to the bottom",
//   "Add counter to the top of the list (todo N, done M)",
//   "Get the list data with async function (delay for 1000 ms, make sure rendering works fine)",
// ];
// export default todoList;
